// option №1
let elements = ["hello", "world", "Kiev", "Kharkiv", "Odessa", "Lviv"];
function createList (arr,element) {
    let parentElement = element || document.body;
    let html = '<ul>';
    arr.forEach(function(item) {
        html += '<li>'+item+'</li>';
    });
    html += '<ul>';
    parentElement.innerHTML = html;
}
createList(elements); 

// option №2 
// let elements = ["hello", "world", "Kiev", "Kharkiv", "Odessa", "Lviv"];

//     function createList (arr, element) { 
//         let parentElement = element || document.body;
//         let html = '<ul>';
//         arr.forEach(function(item) {
//             if (Array.isArray(item)) {

//             } else {
//                 html += '<li>'+item+'</li>';
//             }
//         });
//         html += '</ul>';
        
//         parentElement.innerHTML = html;
//     }

// createList(elements);
